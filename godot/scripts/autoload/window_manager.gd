extends Node

var refresh:bool = false # set this to true to refresh screen

func _ready() -> void:
	get_viewport().connect("size_changed",self,"do_refresh")
	disable_loop()
	refresh = true

func _input(event):
	if event is InputEventMouseMotion:
		refresh = true
	elif event is InputEventMouseButton:
		refresh = true
	pass

func connect_ui( n:Node ) -> void:

	var connect:bool = false
#	if n is Button:
#		connect = true
#	elif n is ScrollContainer:
#		connect = true
#	elif n is SpinBox:
#		connect = true
	
	if connect:
		if n.has_signal("mouse_entered"):
			n.connect("mouse_entered",self,"enable_loop")
		if n.has_signal("mouse_exited"):
			n.connect("mouse_exited",self,"disable_loop")

	for c in n.get_children():
		connect_ui(c)

func do_refresh() -> void:
	print( "wm refresh" )
	refresh = true

func enable_loop() -> void:
	VisualServer.render_loop_enabled = true

func disable_loop() -> void:
	VisualServer.render_loop_enabled = false

func _process(delta) -> void:
	if not VisualServer.render_loop_enabled and refresh:
		VisualServer.force_draw()
		refresh = false
