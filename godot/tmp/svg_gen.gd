extends Node2D

var grid:Vector2 = Vector2(5,6)
var dots:Array = []
var loops:Array = []
var tmp_loop:Array = []
var cell:float = 40
var grey:Color = Color(.3,.3,.3,1)
var empty_connectors:Array = [-1,-1,-1,-1]

func new_dot(x:int,y:int) -> Dictionary:
	return {
		'id': dots.size(),
		'x': x,
		'y': y,
		'active': false,
		'use_count': 0,
		# [rigth, down, left, up]
		# each dot can be connected to maximum 4 other dots
		'connectors': empty_connectors.duplicate(),
		'sprite': null
	}

func _ready():
	
	$grid.columns = grid.x
# warning-ignore:unused_variable
	for i in range(0,(grid.x*grid.y)):
		var b:TextureButton = $btn.duplicate()
		b.visible = true
# warning-ignore:return_value_discarded
		b.connect("pressed",self,"render_svg")
		$grid.add_child(b)
	
	$preview.position = Vector2(250,100)
	for y in range(0,grid.y+1):
		for x in range(0,grid.x+1):
			var d:Dictionary = new_dot(x,y)
			d.sprite = $dot.duplicate()
			d.sprite.visible = true
			d.sprite.modulate = grey
			$preview.add_child(d.sprite)
			d.sprite.position = Vector2(x,y)*cell
			dots.append( d )
	$arrows.position = $preview.position
	$paths.position = $preview.position

func compute_loops( dot:Dictionary = {}, start:int = 0 ) -> void:
	if dot.empty():
		if tmp_loop.size() > 0:
			loops.append( tmp_loop )
		tmp_loop = []
		for d in dots:
			if d.active and d.use_count == 0:
				dot = d
				break
	if dot.empty():
		# no more active dots
		return
	dot.use_count += 1
	tmp_loop.append( dot.id )
	var did:int = dot.id
	var next:int = start
	for i in range(0,4):
		next = (start + i) % 4
		if dot.connectors[next] != -1:
			var d:Dictionary = dot
			dot = dots[d.connectors[next]]
			d.connectors[next] = -1
#			d.connectors = empty_connectors.duplicate()
			break
	if did == dot.id:
		compute_loops()
	else:
		compute_loops(dot,next+3)

func render_svg() -> void:
	
	# reseting dot connections
	for d in dots:
		d.active = false
		d.use_count = 0
		for i in range(0,4):
			d.connectors[i] = -1
	
	# regeneration of connections
	for i in range(0,(grid.x*grid.y)):
		var b:TextureButton = $grid.get_child(i)
		if b.pressed:
			var x:int = i % int(grid.x)
# warning-ignore:integer_division
			var y:int = i / int(grid.x)
			var id0:int = ((x) + (y) * (grid.x+1))
			var id1:int = ((x+1) + (y) * (grid.x+1))
			var id2:int = ((x+1) + (y+1) * (grid.x+1))
			var id3:int = ((x) + (y+1) * (grid.x+1))
			dots[id0].active = true
			dots[id0].connectors[0] = id1
			dots[id1].active = true
			dots[id1].connectors[1] = id2
			dots[id2].active = true
			dots[id2].connectors[2] = id3
			dots[id3].active = true
			dots[id3].connectors[3] = id0
	
	# desactivation of dots with 4 connections
	for d in dots:
		if not d.active:
			continue
		var cnum:int = 0
		for i in range(0,4):
			if d.connectors[i] != -1:
				cnum += 1
		if cnum == 4:
			d.active = false
	
	# computing loops
	loops = []
	tmp_loop = []
	compute_loops()
	
	# display arrows
	while $arrows.get_child_count() > 0:
		$arrows.remove_child($arrows.get_child(0))
	for d in dots:
		if d.active:
			d.sprite.modulate = Color.white
		else:
			d.sprite.modulate = grey
			continue
		for i in range(0,4):
			if d.connectors[i] != -1:
				var dc:Dictionary = dots[d.connectors[i]]
				var dir:Vector2 = Vector2(dc.x-d.x,dc.y-d.y)
				var a:Sprite = null
				if dir.x == 1:
					a = $right.duplicate()
					a.visible = true
				elif dir.x == -1:
					a = $left.duplicate()
					a.visible = true
				if dir.y == 1:
					a = $down.duplicate()
					a.visible = true
				elif dir.y == -1:
					a = $up.duplicate()
					a.visible = true
				if a != null:
					$arrows.add_child(a)
					a.position = d.sprite.position + dir * 12
	
	# display path
	while $paths.get_child_count() > 0:
		$paths.remove_child($paths.get_child(0))
	
	if loops.empty():
		return
	var hgap:float = 1.0 / loops.size()
	var c:Color = Color(1,0,0)
	for l in loops:
		var p:Line2D = $path.duplicate()
		$paths.add_child(p)
		p.default_color = c
		p.visible = true
		p.clear_points()
		for i in range(0,l.size()):
			var di:int = l[i]
			p.add_point(dots[di].sprite.position)
			var ni:int = l[(i+1)%l.size()]
			var d0:Dictionary = dots[di]
			var d1:Dictionary = dots[ni]
			var dir:Vector2 = Vector2( d1.x-d0.x, d1.y-d0.y )
			var a:Sprite = null
			if dir.x == 1:
				a = $right.duplicate()
				a.visible = true
			elif dir.x == -1:
				a = $left.duplicate()
				a.visible = true
			if dir.y == 1:
				a = $down.duplicate()
				a.visible = true
			elif dir.y == -1:
				a = $up.duplicate()
				a.visible = true
			if a != null:
				$paths.add_child(a)
				a.position = d0.sprite.position + (d1.sprite.position-d0.sprite.position) * .5
				a.modulate = c
		c.h += hgap
	
# warning-ignore:unused_argument
func _process(delta):
	pass
