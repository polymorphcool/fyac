extends Node2D

onready var wm:Node = get_node("/root/wm")
onready var fyac:Node = get_node("/root/fyac")

func _ready() -> void:
	wm.connect_ui(self)
	$menu.connect("char_load", self, "load_character")

func load_character( uid:int ) -> void:
	if $menu.autoclose:
		$menu._close()
	$editor.load_char( uid )
	$editor.visible = true
	wm.refresh = true
