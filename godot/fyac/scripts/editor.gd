extends CenterContainer

onready var fyac:Node = get_node("/root/fyac")

export (int) var default_columns:int = 4
export (int) var default_rows:int = 5

var columns:int = default_columns
var rows:int = default_rows

var current_char:Dictionary = {}
var current_variant:int = -1
var cells:int = 0
var lines:Array = []

func _ready():
	get_viewport().connect("size_changed",self,"size_changed")
	$vboard/variants/variant.connect("item_selected",self,"load_variant")
	size_changed()
	build_grid()

func load_char( uid:int ) -> void:
	
	current_char = fyac.get_char( uid )
	current_variant = 0
	
	$vboard/variants/variant.clear()
	if current_char.empty():
		$vboard/info/char.set_text( '?' )
		$vboard/info/ord.set_text( '?' )
		$vboard/info/error.set_text( "invalid character" )
		$vboard/info/error.visible = true
		$vboard/variants.visible = false
		$vboard/grid_wrp.visible = false
		$vboard/bottom.visible = false
		columns = 0
		rows = 0
		return
	
	$vboard/info/char.set_text( char(uid) )
	$vboard/info/ord.set_text( str(uid) )
	$vboard/info/error.visible = false
	$vboard/variants.visible = true
	for i in range(0,current_char.variants.size()):
		$vboard/variants/variant.add_item( str(i) )
	$vboard/grid_wrp.visible = true
	$vboard/bottom.visible = true
	load_variant(0)

func load_variant(i:int):
	if current_char.empty():
		return
	if i < 0 or i >= current_char.variants.size():
		return
	$vboard/variants/rmv_variant.visible = current_char.variants.size() > 1
	current_variant = i
	var variant:Dictionary = current_char.variants[ current_variant ]
	if variant.width <= 0:
		variant.width = default_columns
	if variant.height <= 0:
		variant.height = default_rows
	columns = variant.width
	rows = variant.height
	lines = variant.data
	build_grid()

func save_char() -> void:
	if current_char.empty() or current_variant == -1:
		return
	var variant:Dictionary = current_char.variants[current_variant]
	variant.width = columns
	variant.height = rows
	variant.data = lines
	fyac.set_char( current_char )

func size_changed() -> void:
	self.rect_size = get_viewport().size

func build_grid() -> void:
	cells = columns*rows
	for r in range(0,rows):
		if lines.size() <= r:
			lines.append( [] )
		for c in range(0,columns):
			if lines[r].size() <= c:
				lines[r].append( false )
	var btn_tmpl:TextureButton = $button
	var g:GridContainer = $vboard/grid_wrp/grid
	while( g.get_child_count() > 0 ):
		g.remove_child( g.get_child(0) )
	g.columns = columns
	for r in range(0,rows):
		for c in range(0,columns):
			var b:TextureButton = btn_tmpl.duplicate()
			b.connect("pressed",self,"read_grid")
			b.visible = true
			g.add_child(b)
	refresh_grid()

func refresh_grid() -> void:
	var g:GridContainer = $vboard/grid_wrp/grid
	var i:int = 0
	for btn in g.get_children():
		var r:int = int( i / columns )
		var c:int = int( i % columns )
		btn.pressed = lines[r][c]
		i += 1

func read_grid() -> void:
	var g:GridContainer = $vboard/grid_wrp/grid
	var i:int = 0
	for btn in g.get_children():
		var r:int = int( i / columns )
		var c:int = int( i % columns )
		lines[r][c] = btn.pressed
		i += 1
	save_char()

func _add_col() -> void:
	columns += 1
	build_grid()
	save_char()

func _rmv_col() -> void:
	if ( columns > 1 ):
		columns -= 1
		build_grid()
		save_char()

func _add_row() -> void:
	rows += 1
	build_grid()
	save_char()

func _rmv_row() -> void:
	if ( rows > 1 ):
		rows -= 1
		build_grid()
		save_char()

func _up_col() -> void:
	for r in range(0,rows):
		if r < rows-1:
			for c in range(0,columns):
				lines[r][c] = lines[r+1][c]
		else:
			for c in range(0,columns):
				lines[r][c] = false
	refresh_grid()
	save_char()

func _down_col() -> void:
	for r in range(0,rows):
		var ri = (rows-1)-r
		if ri > 0:
			for c in range(0,columns):
				lines[ri][c] = lines[ri-1][c]
		else:
			for c in range(0,columns):
				lines[ri][c] = false
	refresh_grid()
	save_char()

func _left_row() -> void:
	for c in range(0,columns):
		if c < columns-1:
			for r in range(0,rows):
				lines[r][c] = lines[r][c+1]
		else:
			for r in range(0,rows):
				lines[r][c] = false
	refresh_grid()
	save_char()

func _right_row() -> void:
	for c in range(0,columns):
		var ci = (columns-1)-c
		if ci > 0:
			for r in range(0,rows):
				lines[r][ci] = lines[r][ci-1]
		else:
			for r in range(0,rows):
				lines[r][ci] = false
	refresh_grid()
	save_char()

func _add_variant() -> void:
	if current_char.empty() or current_variant == -1:
		return
	var v:Dictionary = fyac.duplicate_variant(current_char.variants[current_variant])
	current_char.variants.push_back( v )
	current_variant = current_char.variants.size()-1
	$vboard/variants/variant.add_item( str(current_variant) )
	$vboard/variants/variant.selected = current_variant
	load_variant( current_variant )
	save_char()

func _rmv_variant() -> void:
	if current_char.empty() or current_variant == -1:
		return
	if current_char.variants.size() < 2:
		return
	var new_vs = []
	for i in range(0,current_char.variants.size()):
		if i != current_variant:
			new_vs.append( current_char.variants[i] )
	current_char.variants = new_vs
	$vboard/variants/variant.clear()
	for i in range(0,current_char.variants.size()):
		$vboard/variants/variant.add_item(str(i))
	if current_variant >= current_char.variants.size():
		current_variant = current_char.variants.size()-1
	$vboard/variants/variant.selected = current_variant
	load_variant( current_variant )
	save_char()

func _del():
	fyac.char_delete( current_char.uid )
	current_char = {}
	current_variant = -1
	visible = false
