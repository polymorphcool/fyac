extends Node

onready var fyac:Node = get_node("/root/fyac")

onready var file_menu:HBoxContainer = get_node('../wrapper/list/elements/file')
onready var sep:Separator = get_node('../wrapper/list/elements/file_sep')
onready var png_menu:VBoxContainer = get_node('../wrapper/list/elements/png')
onready var svg_menu:VBoxContainer = get_node('../wrapper/list/elements/svg')

onready var png_config:Dictionary = fyac.png_config()
onready var png_fields:Dictionary = {}

func _ready() -> void:
	
	# adapt all configuration
	png_fields.path = png_menu.get_node( "dir/path" )
	png_fields.one_file = png_menu.get_node( "conf/one_file" )
	png_fields.columns = png_menu.get_node( "conf/columns" )
	png_fields.cell = png_menu.get_node( "conf/cell" )
	png_fields.trim = png_menu.get_node( "conf/trim" )
	prepare_png_fields()
	for i in png_fields:
		if png_fields[i] is LineEdit:
			png_fields[i].connect("text_changed", self, "collect_png_fields")
		elif png_fields[i] is SpinBox:
			png_fields[i].connect("value_changed", self, "collect_png_fields")
		elif png_fields[i] is CheckBox:
			png_fields[i].connect("pressed", self, "collect_png_fields")
	
	hide_all()
	$image_viewer.visible = false

func hide_all() -> void:
	sep.visible = false
	png_menu.visible = false
	svg_menu.visible = false

func _on_folder_selected(dir):
	if png_menu.visible:
		png_menu.get_node("dir/path").set_text( dir )
	elif svg_menu.visible:
		svg_menu.get_node("dir/path").set_text( dir )

func _image_viewer_load(imgs:Array) -> void:
	
	var grid:GridContainer = $image_viewer.get_node("list/scroll/center/grid")
	var d:TextureRect = $image_viewer.get_node("list/scroll/center/grid/display")
	d.visible = false
	while grid.get_child_count() > 1:
		grid.remove_child( grid.get_child( grid.get_child_count()-1 ) )
	for i in imgs:
		print(i)
		if not i is Image:
			continue
		var it:ImageTexture = ImageTexture.new()
		it.create_from_image( i, 0 )
		var tr:TextureRect = d.duplicate()
		tr.texture = it
		tr.visible = true
		grid.add_child( tr )
	
	$image_viewer.rect_size = get_viewport().size
	$image_viewer.visible = true

func _image_viewer_close() -> void:
	$image_viewer.visible = false

########## PNG ##########

func prepare_png_fields() -> void:
	png_fields.path.set_text( png_config.path )
	png_fields.one_file.pressed = png_config.one_file
	png_fields.columns.value = png_config.columns
	png_fields.cell.value = png_config.cell
	png_fields.trim.pressed = png_config.trim

func collect_png_fields( v = null ) -> void:
	png_config.path = png_fields.path.text
	png_config.one_file = png_fields.one_file.pressed
	png_config.columns = int(png_fields.columns.value)
	png_config.cell = int(png_fields.cell.value)
	png_config.trim = png_fields.trim.pressed
	print( png_config )

func _export_png() -> void:
	if not png_menu.visible:
		hide_all()
		sep.visible = true
		png_menu.visible = true
	else:
		hide_all()

func _preview_png():
	fyac.export_png( png_config )
	_image_viewer_load( png_config.images )
	png_config.images = []

func _save_png():
	pass

########## JSON ##########

func _export_json() -> void:
	hide_all()

########## TXT ##########

func _export_txt() -> void:
	hide_all()

########## SVG ##########

func _export_svg() -> void:
	if not svg_menu.visible:
		hide_all()
		sep.visible = true
		svg_menu.visible = true
	else:
		hide_all()

func _svg_dialog():
	$folder_dialog.rect_size = get_viewport().size - Vector2(0,25)
	$folder_dialog.popup()
