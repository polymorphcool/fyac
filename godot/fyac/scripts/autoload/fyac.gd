extends Node

signal font_changed

var json_default:String = "res://fyac/data/fyac.json"
var json_path:String = "user://fyac.json"
var cache_path:String = "user://fyac_cache/"
var cache:Dictionary = {}
var fyac_data:Dictionary = {}

func _ready() -> void:
	var d:Directory = Directory.new()
	if !d.dir_exists( cache_path ):
# warning-ignore:return_value_discarded
		d.make_dir( cache_path )
	var f:File = File.new()
	if !f.file_exists( json_path ):
		if !f.file_exists( json_default ):
			fyac_data = json_init()
			var def:File = File.new()
# warning-ignore:return_value_discarded
			def.open( json_default, File.WRITE )
			def.store_string( JSON.print( fyac_data ) )
			def.close()
		var src:File = File.new()
# warning-ignore:return_value_discarded
		src.open( json_default, File.READ )
		var dst:File = File.new()
# warning-ignore:return_value_discarded
		dst.open( json_path, File.WRITE )
		dst.store_string( src.get_as_text() )
		src.close()
		dst.close()
	load_data()

func timestamp() -> String:
	var t = OS.get_datetime()
	var date:String = str(t.year)+'.'
	if t.month < 10:
		date += '0'
	date += str(t.month)+'.'
	if t.day < 10:
		date += '0'
	date += str(t.day)+'.'
	if t.hour < 10:
		date += '0'
	date += str(t.hour)+'.'
	if t.minute < 10:
		date += '0'
	date += str(t.minute)+'.'
	if t.second < 10:
		date += '0'
	date += str(t.second)
	return date

func json_init() -> Dictionary:
	return {
		'font': 'fyac',
		'creation': timestamp(),
		'last_edit': timestamp(),
		'chars': {}
	}

func char_init(uid:int) -> Dictionary:
	return {
		'uid': uid,
		'variants': [
			{
				'width': 0,
				'height': 0,
				'data': []
			}
		]
	}

func load_data() -> void:
	var f:File = File.new()
	# warning-ignore:return_value_discarded
	f.open( json_path, File.READ )
	var content:String = f.get_as_text()
	f.close()
	var res:JSONParseResult = JSON.parse( content )
	if res.error != OK:
		print( "failed to load fyac json! ", res )
# warning-ignore:return_value_discarded
		f.open( json_default, File.READ )
		content = f.get_as_text()
		f.close()
		var e = JSON.parse( content )
		fyac_data = e.result
		return
	fyac_data = res.result

func save_data() -> void:
	var out:File = File.new()
	# warning-ignore:return_value_discarded
	out.open( json_path, File.WRITE )
	out.store_string( JSON.print( fyac_data ) )
	out.close()
	emit_signal("font_changed")

func get_char( uid:int ) -> Dictionary:
	if uid < 0:
		return {}
	for i in fyac_data.chars:
		if fyac_data.chars[i].uid == uid:
			return fyac_data.chars[i]
	fyac_data.chars[uid] = char_init(uid)
	return fyac_data.chars[uid]

func set_char( d:Dictionary ) -> bool:
	if !'uid' in d or !'variants' in d:
		return false
	if d.uid < 0 or d.variants.empty():
		return false
	var valid_variants:Array = []
	for v in  d.variants:
		if 'width' in v and 'height' in v and 'data' in v:
			valid_variants.push_back( v )
	if valid_variants.empty():
		return false
	d.variants = valid_variants
	fyac_data.chars[d.uid] = d
	# regenarting previews
	for i in range(0,d.variants.size()):
# warning-ignore:return_value_discarded
		char_preview(d.uid,i)
		var p:String = char_preview_tex(d.uid,i)
		cache[p] = ResourceLoader.load( p, "", true )
	save_data()
	return true

func duplicate_variant( src:Dictionary ) -> Dictionary:
	var dst:Dictionary = {}
	dst.width = src.width
	dst.height = src.height
	dst.data = []
	for d in src.data:
		var vs:Array = []
		for i in d:
			vs.push_back(i)
		dst.data.push_back(vs)
	return dst

func get_info() -> String:
	if fyac_data.empty():
		return '?'
	var s:String = ''
	s += 'font name: fayc'
	s += '\n' + 'creation ' + fyac_data.creation
	s += '\n' + 'last edit ' + fyac_data.last_edit
	var f:File = File.new()
	# warning-ignore:return_value_discarded
	f.open( json_path, File.READ )
	s += '\n' + f.get_path_absolute()
	s += '\n' + 'characters: ' + str( fyac_data.chars.size() )
	var vs:int = 0
	for c in fyac_data.chars:
		vs += fyac_data.chars[c].variants.size()
	s += ', variants: ' + str(vs)
	return s

func char_exists( uid:int ) -> bool:
	for i in fyac_data.chars:
		if fyac_data.chars[i].uid == uid:
			return true
	return false

func char_preview_png( uid:int, variant:int = 0 ) -> String:
	return cache_path + "char_" + str(uid) + "_" + str(variant) + ".png"

func char_preview_tex( uid:int, variant:int = 0 ) -> String:
	return cache_path + "char_" + str(uid) + "_" + str(variant) + ".tex"

func char_image( uid:int, variant:int = 0 ) -> Image:
	var c = get_char(uid)
	if c.empty():
		return null
	if variant < 0 or variant >= c.variants.size():
		return null
	if c.variants[variant].width * c.variants[variant].height == 0:
		return null
	var i:Image = Image.new()
	i.create( c.variants[variant].width, c.variants[variant].height, false, Image.FORMAT_RGB8 )
	i.lock()
	for y in range( 0, c.variants[variant].height ):
		for x in range( 0, c.variants[variant].width ):
			if c.variants[variant].data[y][x]:
				i.set_pixel(x,y,Color(1,1,1))
			else:
				i.set_pixel(x,y,Color(0,0,0))
	i.unlock()
	return i

func char_preview( uid:int, variant:int = 0 ) -> bool:
	var i:Image = char_image( uid, variant )
	if i == null:
		return false
# warning-ignore:return_value_discarded
	i.save_png( char_preview_png(uid,variant) )
	var t:ImageTexture = ImageTexture.new()
	t.create_from_image( i, 0 )
# warning-ignore:return_value_discarded
	ResourceSaver.save( char_preview_tex(uid,variant), t )
	return true

func char_texture( uid:int, variant:int = 0 ):
	var path = char_preview_tex(uid,variant)
	if not path in cache:
		var f:File = File.new()
		if !f.file_exists( path ):
			var success = char_preview( uid, variant )
			if !success:
				return null
		cache[path] = load( path )
	return cache[path]

func char_delete( uid:int ) -> void:
	if !char_exists( uid ):
		return
	var new_cs = {}
	for i in fyac_data.chars:
		if !fyac_data.chars[i].uid == uid:
			new_cs[fyac_data.chars[i].uid] = fyac_data.chars[i]
	fyac_data.chars = new_cs
	save_data()

func png_config() -> Dictionary:
	var out = {
		'path': '',
		'one_file': true,
		'columns': 5,
		'cell': 5,
		'trim': false,
		'images':[] # < this will be filled with images if path is empty
	}
	return out

# warning-ignore:unused_argument
func export_png( conf:Dictionary ) -> void:
	if not 'path' in conf or not 'one_file' in conf or not 'columns' in conf or not 'cell' in conf or not 'trim' in conf:
		return
	if not 'chars' in fyac_data:
		return
	conf.images = []
	var cs:Array = fyac_data.chars.keys()
	cs.sort()
	for id in cs:
		var c:Dictionary = fyac_data.chars[id]
		if c.variants.size() == 0:
			continue
		var v:Dictionary = c.variants[0]
		var i:Image = char_image( c.uid, 0 )
		if i != null:
			if conf.cell > 1:
				i.resize( v.width*conf.cell, v.height*conf.cell, Image.INTERPOLATE_NEAREST )
			conf.images.append(i)

func generate_svg( uid:int, variant:int = 0 ) -> Dictionary:
	
	var svg_data = {
		"type":"undefined",
		"data": []
	}
	
	var c = get_char(uid)
	if c.empty():
		return svg_data
	if variant < 0 or variant >= c.variants.size():
		return svg_data
	if c.variants[variant].width * c.variants[variant].height == 0:
		return svg_data
		
	print("integrate svg process here!")
	
	return svg_data

func export_svg() -> void:
	if not 'chars' in fyac_data:
		return
	var cs:Array = fyac_data.chars.keys()
	cs.sort()
	for id in cs:
		print( generate_svg(id) )
