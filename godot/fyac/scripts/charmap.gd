extends Node2D

onready var wm:Node = get_node("/root/wm")
onready var fyac:Node = get_node("/root/fyac")

signal char_load

var char_range:Array = [0,0]
var autoclose:bool = false

var presets:Array = [
	{ 'name':'default', 'range':[33,126] },
	{ 'name':'upper', 'range':[65,90] },
	{ 'name':'lower', 'range':[97,122] },
	{ 'name':'num', 'range':[48,57] },
	{ 'name':'256', 'range':[0,256] },
	{ 'name':'512', 'range':[0,512] }
]

func _ready() -> void:
# warning-ignore:return_value_discarded
	get_viewport().connect("size_changed",self,"size_changed")
	size_changed()
# warning-ignore:return_value_discarded
	fyac.connect("font_changed",self,"update_info")
	update_info()
	char_range = presets[0].range
	$wrapper/list/menu/open.visible = false
	$wrapper/list/elements/char_display/min.value = char_range[0]
	$wrapper/list/elements/char_display/max.value = char_range[1]
	for p in presets:
		$wrapper/list/elements/char_display/presets.add_item( p.name )
	autoclose = $wrapper/list/menu/autoclose.pressed
	build_map()

func update_info():
	$wrapper/list/elements/info.set_text( fyac.get_info() )

func build_map() -> void:
	var char_tmpl = $char
	var g:GridContainer = $wrapper/list/elements/scroll/grid
	while( g.get_child_count() > 0 ):
		g.remove_child( g.get_child(0) )
	for cid in range( char_range[0], char_range[1]+1 ):
		var c:PanelContainer = char_tmpl.duplicate()
# warning-ignore:return_value_discarded
		c.connect("char_selected", self,"char_pressed")
		c.char_id = cid
		c.visible = true
		g.add_child( c )
	wm.connect_ui( g )

func char_pressed(uid:int) -> void:
	emit_signal("char_load", uid)

func size_changed() -> void:
	var w:PanelContainer = $wrapper
	w.rect_size.y = get_viewport().size.y

func _min_char(value) -> void:
	char_range[0] = int(value)
	if char_range[0] > char_range[1]:
		char_range[0] = char_range[1]
		$wrapper/list/elements/char_display/min.value = char_range[0]
	build_map()

func _max_char(value) -> void:
	char_range[1] = int(value)
	if char_range[1] < char_range[0]:
		char_range[1] = char_range[0]
		$wrapper/list/elements/char_display/max.value = char_range[1]
	build_map()

func _load_display_preset(index) -> void:
	char_range = presets[index].range
	$wrapper/list/elements/char_display/min.value = char_range[0]
	$wrapper/list/elements/char_display/max.value = char_range[1]
	build_map()

func _close() -> void:
	$wrapper/list/menu/close.visible = false
	$wrapper/list/menu/open.visible = true
	$wrapper/list/elements.visible = $wrapper/list/menu/close.visible
	wm.refresh = true

func _open() -> void:
	$wrapper/list/menu/close.visible = true
	$wrapper/list/menu/open.visible = false
	$wrapper/list/elements.visible = $wrapper/list/menu/close.visible
	wm.refresh = true

func _autoclose():
	autoclose = $wrapper/list/menu/autoclose.pressed
	if !autoclose:
		_open()
	else:
		wm.refresh = true
