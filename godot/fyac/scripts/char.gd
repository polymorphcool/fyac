extends PanelContainer

onready var fyac:Node = get_node("/root/fyac")

export (int) var char_id = 0 setget set_char_id

signal char_selected

func set_char_id(i:int):
	char_id = i
	if is_inside_tree():
		$info/char.set_text( char(char_id) )
		$info/char.rect_size = $info/char.rect_min_size

func _ready():
	set_char_id(char_id)
	$button/btn.connect("pressed",self,"button_pressed")
	fyac.connect("font_changed",self,"update_info")
	update_info()

func button_pressed() -> void:
	emit_signal( "char_selected", char_id )

func update_info():
	if fyac.char_exists( char_id ):
		$info/char.set( "custom_styles/normal", load("res://themes/border_cyan.tres") )
		$display.texture = fyac.char_texture( char_id )
		$display.position = Vector2(32,32)-$display.texture.get_size()
	else:
		$info/char.set( "custom_styles/normal", load("res://themes/border.tres") )
		$display.texture = null
